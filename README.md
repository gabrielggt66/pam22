# PAM22

## gabrielggt66 - ASIXM06 

### Containers

* **pam22:base**  container pam base per practicar les funcions basiques de pam.

```
docker run --rm --name pam.edt.org -h pam.edt.prg --net 2hisx -it kevin16gonzalez/pam22:base
```

* **pam22:ldap**  container te que la funcionalitat  d'autenticació d'usuaris LDAP amb PAM 

```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -d kevin16gonzalez/ldap22:latest
docker run --rm --name pam.edt.org  -h pam.edt.prg  --net 2hisx --privileged -it kevin16gonzalez/pam22:ldap
```
